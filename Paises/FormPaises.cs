﻿namespace Paises
{
    using Modelos;
    using Servicos;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.IO;
    using ImageMagick;

    public partial class FormPaises : Form
    {
        private ServicosApi servicosApi;
        private ServicosInternet servicosInternet;
        private ServicosBaseDados servicosBaseDados;
        private List<Pais> paises;

        public FormPaises()
        {
            InitializeComponent();
            servicosApi = new ServicosApi();
            servicosInternet = new ServicosInternet();
            servicosBaseDados = new ServicosBaseDados();
            paises = new List<Pais>();

            CarregarDados();
        }

        private async void CarregarDados()
        {
            bool carregar;

            //Verificar se existe ligação à internet 
            var ligacao = servicosInternet.VerificarLigacao();

            if (ligacao.Sucesso)
            {
                LabelStatus.Text = "1/3 - A ligar-se à API...";
                await CarregarDadosAPI();
                carregar = true;
            }
            else
            {
                LabelStatus.Text = "1/3 - A ligar-se à Base de Dados local...";
                CarregarDadosBaseDados();
                carregar = false;
            }

            //Verificar se a lista Paises tem dados
            if (paises.Count() == 0)
            {
                MessageBox.Show("Não há ligação à Internet" + Environment.NewLine +
                    "e não foram previamente carregados os paises." + Environment.NewLine +
                    "Tente mais tarde.", "Internet");

                LabelStatus.Text = "Primeira inicialização deverá ter ligação à internet";

                return;
            }

            //Mostrar informação de onde dão carregados os dados
            if (carregar)
            {
                LabelStatus.Text = string.Format("Dados carregados da internet a {0:F}", DateTime.Now);
            }
            else
            {
                LabelStatus.Text = string.Format("Dados carregados da base de dados a {0:F}", File.GetLastWriteTime(@"Dados\Paises.sqlite"));
            }

            carregarComboBoxContinente();

            ComboBoxContinente.SelectedIndex = -1;

            criarColunasListView();
        }

        private void carregarComboBoxContinente()
        {
            //Selectionar os codinetes da lista paises
            var continente = paises.Select(x => x.Region).Distinct().ToList();

            //Acrescentado na lista a opção Todos
            continente.Add("Todos");

            ComboBoxContinente.DataSource = continente;
        }

        private void ComboBoxContinente_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxContinente.SelectedIndex == -1)
            {
                ComboBoxPais.Enabled = false;
                ComboBoxPais.SelectedIndex = -1;
            }
            else
            {
                string continenteSelecionado = ComboBoxContinente.SelectedValue.ToString();

                carregarComboBoxPais(continenteSelecionado);

                ComboBoxPais.Enabled = true;

                ComboBoxPais.SelectedIndex = -1;
                LimparDadosPaisesForm();
            }
        }

        private void carregarComboBoxPais(string selectedItem)
        {
            //Carregar ComboBoxPais
            if (selectedItem == "Todos")
            {
                var paisesFiltroContinente = paises;
                ComboBoxPais.DataSource = paisesFiltroContinente;
            }
            else
            {
                var paisesFiltroContinente = paises.Where(x => x.Region == selectedItem).ToList();
                ComboBoxPais.DataSource = paisesFiltroContinente;
            }
            ComboBoxPais.DisplayMember = "name";
        }

        private void CarregarDadosBaseDados()
        {
            paises = servicosBaseDados.CarregarPaises(ProgressBarStatus);
        }

        private async Task CarregarDadosAPI()
        {
            //Obter dados da Api
            var resposta = await servicosApi.GetPaises("http://restcountries.eu", "/rest/v2/all");

            if (!resposta.Sucesso)
            {
                MessageBox.Show(resposta.Mensagem, "Erro");
                return;
            }

            //Carrega os dados da Api na lista Paises
            paises = (List<Pais>)resposta.Resultado;

            LabelStatus.Text = "2/3 - A guardar dados na base de dados local...";
            LabelStatus.Update(); //Forçar o updadte da label

            servicosBaseDados.ApagarBD();
            servicosBaseDados.GuardarPaises(paises, ProgressBarStatus);

            LabelStatus.Text = "3/3 - A fazer download das bandeiras...";
            LabelStatus.Update();

            int qttPaises = paises.Count;

            servicosBaseDados.downloadBandeiras(ProgressBarStatus, qttPaises, paises);

        }

        private void ComboBoxPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            var paisSelecionado = (Pais)ComboBoxPais.SelectedItem;

            mostrarPaisSelecionado(paisSelecionado);
        }

        private void mostrarPaisSelecionado(Pais paisSelecionado)
        {
            if (!(ComboBoxPais.SelectedIndex == -1))
            {
                GroupBoxPaisStatus.Text = "A atualizar informação do Pais...";
                GroupBoxPaisStatus.Update();

                //Mostrar bandeira

                string caminhoImagem = Application.StartupPath + "\\Imagens\\" + paisSelecionado.Name;

                try
                {
                    using (MagickImage imagemNova = new MagickImage(caminhoImagem + ".svg"))
                    {
                        PictureBoxBandeira.Image = imagemNova.ToBitmap();
                        imagemNova.Dispose();
                    }
                }
                catch
                {
                    MessageBox.Show("Não foi possivel apresentar a bandeira", "Erro converção");
                    PictureBoxBandeira.Image = null;
                }

                TextBoxCapital.Text = paisSelecionado.Capital;
                TextBoxRegiao.Text = paisSelecionado.Region;
                TextBoxPopulacao.Text = paisSelecionado.Population.ToString();

                //Mostrar moedas
                ListViewMoeda.Items.Clear();
                foreach (Moeda moeda in paisSelecionado.Currencies)
                {
                    ListViewItem item;

                    item = ListViewMoeda.Items.Add(moeda.code);
                    item.SubItems.Add(moeda.symbol);
                    item.SubItems.Add(moeda.name);
                }

                ajustarTamanhoColunas(ListViewMoeda);

                //Mostrar dominios internet
                ListViewInternet.Items.Clear();
                foreach (var dominio in paisSelecionado.TopLevelDomain)
                {
                    ListViewItem item;
                    item = ListViewInternet.Items.Add(dominio);
                }

                ajustarTamanhoColunas(ListViewInternet);

                GroupBoxPaisStatus.Text = "Informação do Pais atualizada...";
            }
        }

        private void ajustarTamanhoColunas(ListView listView)
        {
            //Ajustar tamanho das colunas
            listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            //Ajustar tamanho da ultima coluna para ocupar o restante espaço
            if (listView.Columns.Count > 0)
            {
                listView.Columns[listView.Columns.Count - 1].Width = -2;
            }
        }

        private void criarColunasListView()
        {
            //Colulas ListViewMoeda
            ListViewMoeda.Columns.Add("Código");
            ListViewMoeda.Columns.Add("Simbolo");
            ListViewMoeda.Columns.Add("Nome");

            //Colunas ListViewInternet
            ListViewInternet.Columns.Add("Dominio");
        }

        private void ButtonFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autor: Paulo Félix" + Environment.NewLine
                + "Versão: 1.0.0" + Environment.NewLine
                + "Data: 07-12-2017");
        }

        private void ComboBoxContinente_TextUpdate(object sender, EventArgs e)
        {
            //Limpa o form quando a ComboBoc está em alteração
            TextBoxCapital.Text = string.Empty;
            TextBoxRegiao.Text = string.Empty;
            TextBoxPopulacao.Text = string.Empty;
            ComboBoxPais.Text = string.Empty;
            ListViewMoeda.Items.Clear();
            ListViewInternet.Items.Clear();
            GroupBoxPaisStatus.Text = "Tem que escolher um Continente";
            PictureBoxBandeira.Image = null;
            ComboBoxPais.Enabled = false;
        }

        private void ComboBoxPais_TextUpdate(object sender, EventArgs e)
        {
            LimparDadosPaisesForm();
        }

        private void LimparDadosPaisesForm()
        {
            //Limpa o form quando o comboboxPais está em alteração, menos a combobox continente
            TextBoxCapital.Text = string.Empty;
            TextBoxRegiao.Text = string.Empty;
            TextBoxPopulacao.Text = string.Empty;
            ListViewMoeda.Items.Clear();
            ListViewInternet.Items.Clear();
            GroupBoxPaisStatus.Text = "Tem que escolher um Pais";
            PictureBoxBandeira.Image = null;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Desativar os botões da controbox Maximizar e Minimizar
            MaximizeBox = false;
            MinimizeBox = false;
        }
    }
}
