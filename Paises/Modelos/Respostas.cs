﻿namespace Paises.Modelos
{
    public class Respostas
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public object Resultado { get; set; }
    }
}
