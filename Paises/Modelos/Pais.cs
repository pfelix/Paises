﻿namespace Paises.Modelos
{
    using System.Collections.Generic;

    public class Pais
    {
        //Classe para leitura de dados da API
        public int PaisesId { get; set; }
        public string Name { get; set; }
        public List<string> TopLevelDomain { get; set; }
        public string Capital { get; set; }
        public string Region { get; set; }
        public int Population { get; set; }
        public List<Moeda> Currencies { get; set; }
        public string Flag { get; set; }
        //public string Alpha2Code { get; set; }
        //public string Alpha3Code { get; set; }
        //public List<string> CallingCodes { get; set; }
        //public List<string> AltSpellings { get; set; }
        //public string Subregion { get; set; }
        //public List<double> LatLng { get; set; }
        //public string Demonym { get; set; }
        //public double? Area { get; set; }
        //public double? Gini { get; set; }
        //public List<string> Timezones { get; set; }
        //public List<string> Borders { get; set; }
        //public string NativeName { get; set; }
        //public string NumericCode { get; set; }
        //public List<Lingua> Languages { get; set; }
        //public Traducao Translations { get; set; }
        //public List<RegionalBloc> RegionalBlocs { get; set; }
        //public string Cioc { get; set; }
    }
}
