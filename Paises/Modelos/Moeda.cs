﻿namespace Paises.Modelos
{
    public class Moeda
    {
        public string code { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }
    }
}
