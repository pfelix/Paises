﻿namespace Paises.Servicos
{
    using Newtonsoft.Json;
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    /// <summary>
    /// Classe para ligação á Api
    /// </summary>
    public class ServicosApi
    {
        /// <summary>
        /// Obter os dados da Api
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="controlador"></param>
        /// <returns></returns>
        public async Task<Respostas> GetPaises(string urlBase, string controlador)
        {
            try
            {
                var cliente = new HttpClient();

                //Endereço base da API
                cliente.BaseAddress = new Uri(urlBase);

                //Controlador
                var resposta = await cliente.GetAsync(controlador);

                //Guardar o resultado
                var resultado = await resposta.Content.ReadAsStringAsync();

                //Verificar se ligou-se à API
                if (!resposta.IsSuccessStatusCode)
                {
                    return new Respostas
                    {
                        Sucesso = false,
                        Mensagem = resultado
                    };
                }

                //Converte os dados de Json para a lista Pais
                var paises = JsonConvert.DeserializeObject<List<Pais>>(resultado);
                
                //Processo para substituir os null por N/A
                foreach (var pais in paises )
                {
                    if (pais.Capital == null)
                        pais.Capital = "N/A";

                    if (pais.Flag == null)
                        pais.Flag = "N/A";

                    if (pais.Name == null)
                        pais.Name = "N/A";

                    if (pais.Region == null)
                        pais.Region = "N/A";

                    foreach (var moedas in pais.Currencies)
                    {
                        if (moedas.name == null)
                            moedas.name = "N/A";
                        if (moedas.code == null)
                            moedas.code = "N/A";
                        if (moedas.symbol == null)
                            moedas.symbol = "N/A";
                    }

                    //Lista sting temporária para dominios
                    List<string> tmpDominios = new List<string>();

                    foreach (var dominios in pais.TopLevelDomain)
                    {
                        if (dominios == null)
                        {
                            tmpDominios.Add("N/A");
                        }
                        else
                        {
                            tmpDominios.Add(dominios);
                        }
                    }
                    pais.TopLevelDomain = tmpDominios;
                }

                //Manda os dados para o Objeto Resultado da classe Respostas
                return new Respostas
                {
                    Sucesso = true,
                    Resultado = paises
                };
            }
            catch (Exception e)
            {
                return new Respostas
                {
                    Sucesso = false,
                    Mensagem = e.Message
                };
            }
        }
    }
}
