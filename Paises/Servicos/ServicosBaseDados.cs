﻿namespace Paises.Servicos
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using System.Net;
    using System.Windows.Forms;

    public class ServicosBaseDados
    {
        private SQLiteConnection ligacao;
        private SQLiteCommand comando;

        //Contrutor
        public ServicosBaseDados()
        {
            //Cria pasta Dados
            if (!Directory.Exists("Dados"))
            {
                Directory.CreateDirectory("Dados");
            }

            //Caminho e nome da base de dados
            var caminho = @"Dados\Paises.sqlite";
            try
            {
                ligacao = new SQLiteConnection("Data Source =" + caminho);
                ligacao.Open();

                //Criar tabela Paises
                string sqlComando =
                    "create table if not exists paises(idpais integer primary key, name varchar(250), capital varchar(250), region varchar(250), population integer)";

                comando = new SQLiteCommand(sqlComando, ligacao);

                comando.ExecuteNonQuery();

                //Criar tabela moedas
                sqlComando =
                    "create table if not exists moedas(idmoeda integer primary key autoincrement, code varchar(250), name varchar(250), symbal char(2), idpais integer, foreign key (idpais) references paises (idpais))";

                comando = new SQLiteCommand(sqlComando, ligacao);

                comando.ExecuteNonQuery();

                //Criar tabela dominios
                sqlComando =
                    "create table if not exists dominios(iddominio integer primary key autoincrement, dominio varchar(5), idpais integer, foreign key (idpais) references paises (idpais))";

                comando = new SQLiteCommand(sqlComando, ligacao);
                comando.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro Base de dados (Criar tabela)");
            }
        }

        public void GuardarPaises(List<Pais> Paises, ProgressBar progressBar)
        {
            int linhas = Paises.Count;

            //Para mostar a progressBar a aumentar
            progressBar.Maximum = linhas;
            progressBar.Value = 0;

            try
            {
                //Guardar dados na tabela Paises
                int idpais = 1; //Para fazer a ligação entre as tabelas

                foreach (var pais in Paises)
                {
                    string sqlComando =
                        string.Format("insert into paises (idpais, name, capital, region, population) values (@idpais, @name,@capital,@region,@population)");


                    comando = new SQLiteCommand(sqlComando, ligacao);
                    comando.Parameters.AddWithValue("@idpais", idpais);
                    comando.Parameters.AddWithValue("@name", pais.Name);
                    comando.Parameters.AddWithValue("@capital", pais.Capital);
                    comando.Parameters.AddWithValue("@region", pais.Region);
                    comando.Parameters.AddWithValue("@population", pais.Population);

                    comando.ExecuteNonQuery();

                    //Guardar dados na tabela moedas
                    foreach (var moeda in pais.Currencies)
                    {
                        sqlComando = string.Format("insert into moedas (code, name, symbal, idpais) values (@code, @name, @symbal, @idpais)");

                        comando = new SQLiteCommand(sqlComando, ligacao);
                        comando.Parameters.AddWithValue("@code", moeda.code);
                        comando.Parameters.AddWithValue("@name", moeda.name);
                        comando.Parameters.AddWithValue("@symbal", moeda.symbol);
                        comando.Parameters.AddWithValue("@idpais", idpais);

                        comando.ExecuteNonQuery();
                    }

                    //Guardar dados na tabela dominio
                    foreach (var dominio in pais.TopLevelDomain)
                    {
                        sqlComando = string.Format("insert into dominios(dominio, idpais) values (@dominio, @idpais)");

                        comando = new SQLiteCommand(sqlComando, ligacao);
                        comando.Parameters.AddWithValue("@dominio", dominio);
                        comando.Parameters.AddWithValue("@idpais", idpais);

                        comando.ExecuteNonQuery();
                    }

                    idpais++;

                    progressBar.Value += 1;
                }
                ligacao.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro Base de dados (Guardar)");
            }
        }

        internal void ApagarBD()
        {
            try
            {
                string sqlComando = string.Format("delete from Paises; delete from moedas; delete from dominios; update sqlite_sequence set seq = 0");
                comando = new SQLiteCommand(sqlComando, ligacao);
                comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro Base de dados (Apagar)");
            }
        }

        public List<Pais> CarregarPaises(ProgressBar progressBar)
        {
            List<Pais> paises = new List<Pais>();

            progressBar.Value = 0;

            try
            {
                //Saber a quantidade de linhas que existem na tabela
                string sqlComando =
                    "select count(*) from paises";

                comando = new SQLiteCommand(sqlComando, ligacao);

                var linha = comando.ExecuteScalar();
                
                //Meter o valor máximo da progressBar igual ao numero de linhas encontrato
                progressBar.Maximum = Convert.ToInt32(linha);

                //Select tabela paises
                sqlComando = "select idpais, name, capital, region, population from paises";

                comando = new SQLiteCommand(sqlComando, ligacao);

                //Lê cada registo (linha)
                SQLiteDataReader reader = comando.ExecuteReader();

                //Carrega da BD para a lista
                while (reader.Read())
                {
                    paises.Add(new Pais
                    {
                        PaisesId = Convert.ToInt32(reader["idpais"]),
                        Name = (string)reader["name"],
                        Capital = (string)reader["capital"],
                        Region = (string)reader["region"],
                        Population = Convert.ToInt32(reader["population"]),
                        Currencies = CarregarMoedas(Convert.ToInt32(reader["idpais"])),
                        TopLevelDomain = CarregarDominios(Convert.ToInt32(reader["idpais"])),
                    });
                    progressBar.Value += 1;
                }
                ligacao.Close();

                return paises;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro ao ler tabela Paises");

                return null;
            }
        }

        private List<string> CarregarDominios(int IdPais)
        {
            //Para passar da tabela para a lista
            List<string> dominios = new List<string>();

            try
            {
                string sqlComando =
                    "select dominio from dominios where idpais =" + IdPais;

                comando = new SQLiteCommand(sqlComando, ligacao);

                //Lê cada registo (linha)
                SQLiteDataReader reader = comando.ExecuteReader();

                //Carrega da BD para a lista
                while (reader.Read())
                {
                    dominios.Add(reader["dominio"].ToString());
                }
                return dominios;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro ao ler da tabela Dominios");

                return null;
            }
        }

        private List<Moeda> CarregarMoedas(int IdPais)
        {
            //Para passar da tabela para a lista
            List<Moeda> moedas = new List<Moeda>();

            try
            {
                string sqlComando =
                    "select code, name, symbal from moedas where idpais =" + IdPais;

                comando = new SQLiteCommand(sqlComando, ligacao);

                //Lê cada registo (linha)
                SQLiteDataReader reader = comando.ExecuteReader();

                //Carrega da BD para a lista
                while (reader.Read())
                {
                    moedas.Add(new Moeda
                    {
                        code = (string)reader["code"],
                        name = (string)reader["name"],
                        symbol = (string)reader["symbal"],
                    });
                }
                return moedas;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro ao ler da tabela Moedas");

                return null;
            }
        }

        public void downloadBandeiras(ProgressBar progressBar, int quantidadePaises, List<Pais> listaPaises)
        {
            progressBar.Maximum = quantidadePaises;

            progressBar.Value = 0;

            //Criar pasta Imagens
            if (!Directory.Exists("Imagens"))
            {
                Directory.CreateDirectory("Imagens");
            }

            try
            {
                foreach (Pais pais in listaPaises)
                {
                    string caminhoImagem = Application.StartupPath + "\\Imagens\\" + pais.Name + ".svg";

                    //Download bandeiras
                    using (WebClient webClient = new WebClient())
                    {
                        webClient.DownloadFile(pais.Flag, caminhoImagem);
                    }

                    progressBar.Value += 1;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro download bandeiras");
            }
        }

    }
}
