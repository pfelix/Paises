﻿namespace Paises.Servicos
{
    using Modelos;
    using System.Net;

    public class ServicosInternet
    {
        public Respostas VerificarLigacao()
        {
            //Devolve um objeto do tipo Response
            var client = new WebClient();

            try
            {
                using (client.OpenRead("http://clients3.google.com/generate_204"))// Verificar se tem ligação á net
                {
                    return new Respostas
                    {
                        Sucesso = true
                    };
                }
            }
            catch
            {
                return new Respostas
                {
                    Sucesso = false,
                    Mensagem = "Configure a sua ligação à Internet."
                };
            }
        }


    }
}
