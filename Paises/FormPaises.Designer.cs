﻿namespace Paises
{
    partial class FormPaises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPaises));
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ComboBoxPais = new System.Windows.Forms.ComboBox();
            this.TextBoxCapital = new System.Windows.Forms.TextBox();
            this.TextBoxRegiao = new System.Windows.Forms.TextBox();
            this.TextBoxPopulacao = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PictureBoxBandeira = new System.Windows.Forms.PictureBox();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ListViewMoeda = new System.Windows.Forms.ListView();
            this.ListViewInternet = new System.Windows.Forms.ListView();
            this.ProgressBarStatus = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.ComboBoxContinente = new System.Windows.Forms.ComboBox();
            this.GroupBoxPaisStatus = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBandeira)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "País";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Capital";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Região";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "População";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(378, 252);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "Dominio internet";
            // 
            // ComboBoxPais
            // 
            this.ComboBoxPais.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComboBoxPais.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBoxPais.Enabled = false;
            this.ComboBoxPais.FormattingEnabled = true;
            this.ComboBoxPais.Location = new System.Drawing.Point(106, 84);
            this.ComboBoxPais.Name = "ComboBoxPais";
            this.ComboBoxPais.Size = new System.Drawing.Size(408, 21);
            this.ComboBoxPais.TabIndex = 4;
            this.ComboBoxPais.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPais_SelectedIndexChanged);
            this.ComboBoxPais.TextUpdate += new System.EventHandler(this.ComboBoxPais_TextUpdate);
            // 
            // TextBoxCapital
            // 
            this.TextBoxCapital.Location = new System.Drawing.Point(106, 139);
            this.TextBoxCapital.Name = "TextBoxCapital";
            this.TextBoxCapital.ReadOnly = true;
            this.TextBoxCapital.Size = new System.Drawing.Size(165, 20);
            this.TextBoxCapital.TabIndex = 7;
            // 
            // TextBoxRegiao
            // 
            this.TextBoxRegiao.Location = new System.Drawing.Point(106, 178);
            this.TextBoxRegiao.Name = "TextBoxRegiao";
            this.TextBoxRegiao.ReadOnly = true;
            this.TextBoxRegiao.Size = new System.Drawing.Size(165, 20);
            this.TextBoxRegiao.TabIndex = 9;
            // 
            // TextBoxPopulacao
            // 
            this.TextBoxPopulacao.Location = new System.Drawing.Point(106, 217);
            this.TextBoxPopulacao.Name = "TextBoxPopulacao";
            this.TextBoxPopulacao.ReadOnly = true;
            this.TextBoxPopulacao.Size = new System.Drawing.Size(165, 20);
            this.TextBoxPopulacao.TabIndex = 11;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobreToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(544, 24);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.sobreToolStripMenuItem.Text = "Sobre...";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // PictureBoxBandeira
            // 
            this.PictureBoxBandeira.BackColor = System.Drawing.Color.Transparent;
            this.PictureBoxBandeira.Location = new System.Drawing.Point(296, 139);
            this.PictureBoxBandeira.Name = "PictureBoxBandeira";
            this.PictureBoxBandeira.Size = new System.Drawing.Size(218, 97);
            this.PictureBoxBandeira.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxBandeira.TabIndex = 16;
            this.PictureBoxBandeira.TabStop = false;
            // 
            // LabelStatus
            // 
            this.LabelStatus.BackColor = System.Drawing.Color.Transparent;
            this.LabelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(27, 405);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(419, 24);
            this.LabelStatus.TabIndex = 16;
            this.LabelStatus.Text = "Status";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(25, 252);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Moeda";
            // 
            // ListViewMoeda
            // 
            this.ListViewMoeda.GridLines = true;
            this.ListViewMoeda.Location = new System.Drawing.Point(28, 270);
            this.ListViewMoeda.Name = "ListViewMoeda";
            this.ListViewMoeda.Size = new System.Drawing.Size(323, 94);
            this.ListViewMoeda.TabIndex = 13;
            this.ListViewMoeda.UseCompatibleStateImageBehavior = false;
            this.ListViewMoeda.View = System.Windows.Forms.View.Details;
            // 
            // ListViewInternet
            // 
            this.ListViewInternet.GridLines = true;
            this.ListViewInternet.Location = new System.Drawing.Point(381, 270);
            this.ListViewInternet.Name = "ListViewInternet";
            this.ListViewInternet.Scrollable = false;
            this.ListViewInternet.Size = new System.Drawing.Size(131, 94);
            this.ListViewInternet.TabIndex = 15;
            this.ListViewInternet.UseCompatibleStateImageBehavior = false;
            this.ListViewInternet.View = System.Windows.Forms.View.Details;
            // 
            // ProgressBarStatus
            // 
            this.ProgressBarStatus.Location = new System.Drawing.Point(28, 379);
            this.ProgressBarStatus.Name = "ProgressBarStatus";
            this.ProgressBarStatus.Size = new System.Drawing.Size(486, 23);
            this.ProgressBarStatus.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(25, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Continente";
            // 
            // ComboBoxContinente
            // 
            this.ComboBoxContinente.AccessibleName = "";
            this.ComboBoxContinente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComboBoxContinente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBoxContinente.FormattingEnabled = true;
            this.ComboBoxContinente.Location = new System.Drawing.Point(106, 44);
            this.ComboBoxContinente.Name = "ComboBoxContinente";
            this.ComboBoxContinente.Size = new System.Drawing.Size(408, 21);
            this.ComboBoxContinente.TabIndex = 2;
            this.ComboBoxContinente.SelectedIndexChanged += new System.EventHandler(this.ComboBoxContinente_SelectedIndexChanged);
            this.ComboBoxContinente.TextUpdate += new System.EventHandler(this.ComboBoxContinente_TextUpdate);
            // 
            // GroupBoxPaisStatus
            // 
            this.GroupBoxPaisStatus.BackColor = System.Drawing.Color.Transparent;
            this.GroupBoxPaisStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.GroupBoxPaisStatus.Location = new System.Drawing.Point(12, 114);
            this.GroupBoxPaisStatus.Name = "GroupBoxPaisStatus";
            this.GroupBoxPaisStatus.Size = new System.Drawing.Size(520, 259);
            this.GroupBoxPaisStatus.TabIndex = 23;
            this.GroupBoxPaisStatus.TabStop = false;
            this.GroupBoxPaisStatus.Text = "Informação do Pais";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Paises.Properties.Resources.World_Map_PNG_Photos;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(544, 438);
            this.Controls.Add(this.ComboBoxContinente);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ProgressBarStatus);
            this.Controls.Add(this.ListViewInternet);
            this.Controls.Add(this.ListViewMoeda);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.PictureBoxBandeira);
            this.Controls.Add(this.TextBoxPopulacao);
            this.Controls.Add(this.TextBoxRegiao);
            this.Controls.Add(this.TextBoxCapital);
            this.Controls.Add(this.ComboBoxPais);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.GroupBoxPaisStatus);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paises";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBandeira)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox ComboBoxPais;
        private System.Windows.Forms.TextBox TextBoxCapital;
        private System.Windows.Forms.TextBox TextBoxRegiao;
        private System.Windows.Forms.TextBox TextBoxPopulacao;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.PictureBox PictureBoxBandeira;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListView ListViewMoeda;
        private System.Windows.Forms.ListView ListViewInternet;
        private System.Windows.Forms.ProgressBar ProgressBarStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboBoxContinente;
        private System.Windows.Forms.GroupBox GroupBoxPaisStatus;
    }
}

